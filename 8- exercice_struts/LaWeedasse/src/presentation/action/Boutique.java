package presentation.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import fr.afpa.dto.WeedDto;
import fr.afpa.service.ServiceFactory;
import fr.afpa.service.interfaces.IWeedService;

/**
 * Servlet implementation class Boutique
 */
public class Boutique extends Action {
	private static final long serialVersionUID = 1L;
	private IWeedService weedService;
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return mapping.findForward("success");
	}

//
//	@Override
//	public void init() throws ServletException {
//		super.init();
//		ServiceFactory serviceFactory = new ServiceFactory();
//		weedService = serviceFactory.getWeedService(ServiceFactory.TYPE_WEED_SERVICE_BDD);
//	}
//
//	/**
//	 * @see HttpServlet#HttpServlet()
//	 */
//	public Boutique() {
//		super();
//		// TODO Auto-generated constructor stub
//	}
//
//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
//	 *      response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		
//		List<WeedDto> weeds = weedService.getAllWeed();
//		
//		request.setAttribute("weeds", weeds);
//		request.getRequestDispatcher("/jsp/Boutique.jsp").forward(request, response);
//		
//	}
//
//	/**
//	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
//	 *      response)
//	 */
//	protected void doPost(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		doGet(request, response);
//	}
//
//	@Override
//	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//
//		super.service(req, resp);
//
//	}
//
//	@Override
//	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//
//		super.service(req, res);
//
//	}
}
