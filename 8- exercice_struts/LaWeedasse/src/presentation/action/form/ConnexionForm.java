package presentation.action.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class ConnexionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7269134651469544306L;

	private String email;
	private String password;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		final ActionErrors errors = new ActionErrors();
		// Id obligatoire
		if (email.isEmpty()) {
			// TODO XSI : besoin de mettre les 2 ?
			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.connexion.email.obligatoire"));
			
		}

		// mail valide
		if (!email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.connexion.email.invalide"));
		}

		// Id obligatoire
		if (password.isEmpty()) {
			// TODO XSI : besoin de mettre les 2 ?
			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.connexion.password.obligatoire"));
		
		}

//		// Id obligatoire
//		if (password.length()<6 ||  password.length()>15) {
//			// TODO XSI : besoin de mettre les 2 ?
//			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.connexion.password.taille"));
//			
//		}

//		// mail valide
//		if (!password.matches("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{8,15})")) {
//			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.connexion.password.invalide1"));
//			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.connexion.password.invalide2"));
//			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.connexion.password.invalide3"));
//			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.connexion.password.invalide4"));
//			errors.add(ActionErrors.GLOBAL_MESSAGE, new ActionMessage("error.connexion.password.invalide5"));
//		}

		return errors;

	}

}
