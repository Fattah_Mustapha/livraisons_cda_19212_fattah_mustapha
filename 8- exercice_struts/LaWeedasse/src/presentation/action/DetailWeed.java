package presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import fr.afpa.service.interfaces.IWeedService;

/**
 * Servlet implementation class DetailWeed
 */
public class DetailWeed extends Action {
	private static final long serialVersionUID = 1L;
	private IWeedService weedService;
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return mapping.findForward("success");
	}
	
	

//	@Override
//	public void init() throws ServletException {
//		super.init();
//		ServiceFactory serviceFactory = new ServiceFactory();
//		weedService = serviceFactory.getWeedService(ServiceFactory.TYPE_WEED_SERVICE_BDD);
//	}
//
//	/**
//	 * @see HttpServlet#HttpServlet()
//	 */
//	public DetailWeed() {
//		super();
//		// TODO Auto-generated constructor stub
//	}
//
//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
//	 *      response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		
//		String idWeed = request.getParameter("id");
//		Integer id = Integer.parseInt(idWeed);
//		WeedDto weed = weedService.chercherUneWeed(id);
//		request.setAttribute("weed", weed);
//		request.getRequestDispatcher("/jsp/DetailWeed.jsp").forward(request, response);
//	}
//
//	/**
//	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
//	 *      response)
//	 */
//	protected void doPost(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		doGet(request, response);
//	}

}
