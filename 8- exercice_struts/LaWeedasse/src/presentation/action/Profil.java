package presentation.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import fr.afpa.dto.UserDto;
import fr.afpa.service.ServiceFactory;
import fr.afpa.service.interfaces.IUserService;

/**
 * Servlet implementation class Profil
 */
public class Profil extends Action {
	private static final long serialVersionUID = 1L;
	private IUserService userService;
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return mapping.findForward("success");
	}

//	@Override
//	public void init() throws ServletException {
//		super.init();
//		ServiceFactory serviceFactory = new ServiceFactory();
//		userService = serviceFactory.getUserService(ServiceFactory.TYPE_USER_SERVICE_BDD);
//	}
//
//	/**
//	 * @see HttpServlet#HttpServlet()
//	 */
//	public Profil() {
//		super();
//		// TODO Auto-generated constructor stub
//	}
//
//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
//	 *      response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		
//		String nom = request.getParameter("nom");
//		UserDto user = userService.chercherUnUser(nom);
//		request.setAttribute("user", user);
//		request.getRequestDispatcher("/jsp/Profil.jsp").forward(request, response);
//	}
//
//	/**
//	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
//	 *      response)
//	 */
//	protected void doPost(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		
//		HttpSession session = request.getSession();
//		session.invalidate();
//		request.getRequestDispatcher("/jsp/Accueil.jsp").forward(request, response);
//
//	}

}
