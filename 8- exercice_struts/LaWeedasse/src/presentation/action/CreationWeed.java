package presentation.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import fr.afpa.dto.WeedDto;
import fr.afpa.service.ServiceFactory;
import fr.afpa.service.interfaces.IWeedService;

/**
 * Servlet implementation class CreationWeed
 */
public class CreationWeed extends Action {
	private static final long serialVersionUID = 1L;
	private IWeedService weedService;
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return mapping.findForward("success");
	}

//	@Override
//	public void init() throws ServletException {
//		super.init();
//		ServiceFactory serviceFactory = new ServiceFactory();
//		weedService = serviceFactory.getWeedService(ServiceFactory.TYPE_WEED_SERVICE_BDD);
//	}
//
//	/**
//	 * @see HttpServlet#HttpServlet()
//	 */
//	public CreationWeed() {
//		super();
//		// TODO Auto-generated constructor stub
//	}
//
//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
//	 *      response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		request.getRequestDispatcher("/jsp/CreationWeed.jsp").forward(request, response);
//	}
//
//	private int parseInt(String prix2) {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	/**
//	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
//	 *      response)
//	 */
//	protected void doPost(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		
//		String id = request.getParameter("id");
//		String nom = request.getParameter("nom");
//		String prix = request.getParameter("prix");
//		String poid = request.getParameter("poid");
//		try {
//			WeedDto w = new WeedDto(parseInt(id), nom, parseInt(prix), parseInt(poid));
//
//			weedService.addWeed(w);
//
//			response.sendRedirect(request.getContextPath() + "/Boutique");
//
//		} catch (NumberFormatException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

}
