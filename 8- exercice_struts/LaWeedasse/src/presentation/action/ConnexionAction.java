package presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import fr.afpa.dto.UserDto;
import fr.afpa.presentation.actionform.ConnexionForm;
import fr.afpa.service.ServiceFactory;
import fr.afpa.service.interfaces.IUserService;

/**
 * Servlet implementation class Accueil
 */
public class ConnexionAction extends Action {
	private IUserService userService=new ServiceFactory().getUserService(ServiceFactory.TYPE_USER_SERVICE_BDD);;


	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {			
		
		
		ConnexionForm connexionForm = (ConnexionForm) form;

		UserDto user = userService.chercherUserParLoginPassword(connexionForm.getEmail(), connexionForm.getPassword());

		if (user == null) {
			request.setAttribute("messageErreur", "Login/password incorrects");
			return mapping.findForward("echec");
		} else {
			request.getSession(true).setAttribute("connectedUser", user);
			return mapping.findForward("success");
		}
		
		
		
	}

	
	


}
