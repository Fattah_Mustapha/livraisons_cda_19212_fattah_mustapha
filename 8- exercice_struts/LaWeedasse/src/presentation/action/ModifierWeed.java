package presentation.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import fr.afpa.dto.WeedDto;
import fr.afpa.service.ServiceFactory;
import fr.afpa.service.interfaces.IWeedService;

/**
 * Servlet implementation class ModifierWeed
 */
public class ModifierWeed extends Action {
	private static final long serialVersionUID = 1L;
	private IWeedService weedService;
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return mapping.findForward("success");
	}
	
	

//	@Override
//	public void init() throws ServletException {
//		super.init();
//		ServiceFactory serviceFactory = new ServiceFactory();
//		weedService = serviceFactory.getWeedService(ServiceFactory.TYPE_WEED_SERVICE_BDD);
//	}
//
//	/**
//	 * @see HttpServlet#HttpServlet()
//	 */
//	public ModifierWeed() {
//		super();
//		// TODO Auto-generated constructor stub
//	}
//
//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
//	 *      response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		
//		WeedDto weed = weedService.chercherUneWeed(Integer.parseInt(request.getParameter("id")));
//		request.setAttribute("weed", weed);
//		request.getRequestDispatcher("/jsp/ModifierWeed.jsp").forward(request, response);
//	}
//
//	/**
//	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
//	 *      response)
//	 */
//	protected void doPost(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		
//		// TODO Auto-generated method stub
//		Integer id = Integer.valueOf(request.getParameter("id"));
//		String nom = request.getParameter("nom");
//		Integer prix = Integer.valueOf(request.getParameter("prix"));
//		Float poid = Float.valueOf(request.getParameter("poid"));
//
//		WeedDto newWeed = new WeedDto(id, nom, prix, poid);
//		weedService.updateArticle(newWeed);
//
//		response.sendRedirect(request.getContextPath() + "/Boutique");
//	}

}
