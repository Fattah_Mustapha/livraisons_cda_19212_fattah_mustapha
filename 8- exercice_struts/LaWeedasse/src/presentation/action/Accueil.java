package presentation.action;

import java.io.IOException;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Accueil
 */
public class Accueil extends Action {
	private static final long serialVersionUID = 1L;

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return mapping.findForward("success");
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
//	public Accueil() {
//		super();
//		// TODO Auto-generated constructor stub
//	}
//
//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
//	 *      response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		request.getRequestDispatcher("/jsp/Accueil.jsp").forward(request, response);
//	}
//
//	/**
//	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
//	 *      response)
//	 */
//	protected void doPost(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		String nom = request.getParameter("nom");
//		String mdp = request.getParameter("mdp");
//
//		HttpSession session = request.getSession();
//
//		session.setAttribute("nom", nom);
//		session.setAttribute("mdp", mdp);
//		// request.getRequestDispatcher("/src/fr/afpa/servlet/Connexion").forward(request,
//		// response);
//		request.getRequestDispatcher("/Connexion").forward(request, response);
//
//	}

}
