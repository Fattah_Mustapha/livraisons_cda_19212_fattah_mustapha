package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.service.ServiceFactory;
import fr.afpa.service.interfaces.IWeedService;

/**
 * Servlet implementation class SupprimerWeed
 */
public class SupprimerWeed extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IWeedService weedService;

	@Override
	public void init() throws ServletException {
		super.init();
		ServiceFactory serviceFactory = new ServiceFactory();
		weedService = serviceFactory.getWeedService(ServiceFactory.TYPE_WEED_SERVICE_BDD);
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SupprimerWeed() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idString = request.getParameter("id");

		Integer id = Integer.parseInt(idString);

		weedService.deleteWeed(id);

		response.sendRedirect(request.getContextPath() + "/Boutique");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
