package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.dto.UserDto;
import fr.afpa.service.ServiceFactory;
import fr.afpa.service.interfaces.IUserService;

/**
 * Servlet implementation class Connexion
 */
public class Connexion extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IUserService userService;

	@Override
	public void init() throws ServletException {
		super.init();
		ServiceFactory serviceFactory = new ServiceFactory();
		userService = serviceFactory.getUserService(ServiceFactory.TYPE_USER_SERVICE_BDD);
	}
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Connexion() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/Accueil").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String email = request.getParameter("nom");
		String mdpUser = request.getParameter("mdp");

		UserDto user = userService.chercherUserParLoginPassword(email, mdpUser);

		if (user == null) {
			request.setAttribute("messageErreur", "Login/password incorrects");
		} else {
			request.getSession(true).setAttribute("connectedUser", user);
		}
		request.getRequestDispatcher("jsp/Accueil.jsp").forward(request, response);
	}

}
