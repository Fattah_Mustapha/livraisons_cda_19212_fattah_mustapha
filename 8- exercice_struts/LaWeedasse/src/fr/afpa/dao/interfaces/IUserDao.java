package fr.afpa.dao.interfaces;

import java.util.List;

import fr.afpa.entities.User;

public interface IUserDao {

	public List<User> getAllUsers();

	public User chercherUnUser(Integer id);

	public User chercherUnUser(String prenomP);

	public void deleteUser(Integer id);

	public void save(User user);

	public void update(User user);

	public User chercherUserParLoginPassword(String login, String password);

}
