package fr.afpa.service;

import java.util.List;

import fr.afpa.dao.DaoFactory;
import fr.afpa.dao.interfaces.IUserDao;
import fr.afpa.dto.UserDto;
import fr.afpa.entities.User;
import fr.afpa.service.interfaces.IUserService;
import fr.afpa.service.mappers.UserMapper;

public class UserServiceBDD implements IUserService {

	IUserDao userDao;
	private UserMapper userMapper = new UserMapper();

	public UserServiceBDD() {
		super();
		DaoFactory daoFactory = new DaoFactory();
		userDao = daoFactory.getUserDao(DaoFactory.TYPE_USER_DAO_PSQL);
	}

	@Override
	public List<UserDto> getAllUsers() {

		List<User> users = userDao.getAllUsers();

		return userMapper.mapToDtos(users);

	}

	@Override
	public UserDto chercherUnUser(Integer id) {
		User user = userDao.chercherUnUser(id);
		return userMapper.mapToDto(user);
	}

	@Override
	public UserDto chercherUnUser(String nom) {
		User user = userDao.chercherUnUser(nom);
		return userMapper.mapToDto(user);
	}

	@Override
	public void addUser(UserDto userDto) {

		User user = userMapper.mapToDo(userDto);

		userDao.save(user);
	}

	@Override
	public void deleteUser(UserDto userDto) {
		User user = userMapper.mapToDo(userDto);
		deleteUser(user.getId());
	}

	@Override
	public void deleteUser(Integer id) {
		userDao.deleteUser(id);
	}

	@Override
	public UserDto chercherUserParLoginPassword(String login, String password) {

		User user = userDao.chercherUserParLoginPassword(login, password);
		return userMapper.mapToDto(user);
	}

}
