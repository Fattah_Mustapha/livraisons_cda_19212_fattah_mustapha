package fr.afpa.service.interfaces;

import java.util.List;

import fr.afpa.dto.UserDto;

public interface IUserService {
	public List<UserDto> getAllUsers();

	public UserDto chercherUnUser(Integer id);

	public UserDto chercherUnUser(String nom);

	public void addUser(UserDto user);

	public void deleteUser(UserDto user);

	public void deleteUser(Integer id);

	public UserDto chercherUserParLoginPassword(String login, String password);
}
