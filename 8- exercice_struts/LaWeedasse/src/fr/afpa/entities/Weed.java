package fr.afpa.entities;

public class Weed {
	int id;
	String nom;
	int prix;
	float poid;

	public Weed() {
		super();
	}

	public Weed(int id, String nom, int prix, float poid) {
		super();
		this.id = id;
		this.nom = nom;
		this.prix = prix;
		this.poid = poid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public float getPoid() {
		return poid;
	}

	public void setPoid(float poid) {
		this.poid = poid;
	}
}
