package fr.afpa.presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import fr.afpa.dto.UserDto;
import fr.afpa.presentation.actionform.ConnexionForm;
import fr.afpa.service.ServiceFactory;
import fr.afpa.service.interfaces.IUserService;

/**
 * Servlet implementation class Accueil
 */
public class AfficherConnexionAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {			
			
		return mapping.findForward("success");
		
	}

	
	


}
