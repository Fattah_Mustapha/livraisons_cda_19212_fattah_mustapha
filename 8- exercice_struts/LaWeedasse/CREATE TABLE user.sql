--CREATE DATABase bdd_weed_jovany; 


CREATE TABLE users(
    id serial PRIMARY KEY NOT NULL,
    prenom VARCHAR,
    mdp VARCHAR,
    nom VARCHAR,
    adresse VARCHAR,
    tel numeric,
    email VARCHAR
);

CREATE TABLE weeds(
    id serial PRIMARY KEY NOT NULL,
    nom VARCHAR,
    prix numeric,
    poid numeric(6,2)
);
INSERT INTO users (id,prenom,mdp,nom,adresse,tel,email)
VALUES   (1,'Tabouley',md5('azerty'),'Lemoine','47 rue kepler',41,'Lemoine@gmail.com'),
        (2,'Eloise',md5('qsdfghs'),'Carton','47 rue kepler',87,'Eloise@gmail.com'),
        (3,'Linux',md5('wxcvbn'),'Babine','47 rue kepler',60,'Linux@gmail.com');
INSERT INTO weeds (id,nom,prix,poid)
VALUES (1,'Cookie Kush',10,1.0),
(2,'Orange Bud',18,0.7),
(3,'Lemon Skunk',12,1.1);
