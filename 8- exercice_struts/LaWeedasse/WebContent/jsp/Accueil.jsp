<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%-- <%@ page import="presentation.action.Accueil" %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> 
 <link crossorigin="anonymous"        href="https://maxcdn.bootstrapcdn.com/
bootstrap/3.3.7/css/bootstrap.min.css" integrity=
"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" 
rel="stylesheet"> 
 <link crossorigin="anonymous"        href="https://maxcdn.bootstrapcdn.com/
bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity=
"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" 
rel="stylesheet"> 
         
 <script crossorigin="anonymous" integrity=
"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" 
src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">
</script> 

<title>La Weedasse</title>
</head>
<body>
  <nav class="navbar navbar-inverse navbar-fixed-top"> 
      <div class="container"> 
      <div class="col-lg-12">
      <img alt="Banniere" src="/static/img/Banweed.png"/>
      </div>
        <div class="navbar-header"> 
          <button type="button" class="navbar-toggle collapsed" data-toggle=
"collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> 
            <span class="sr-only">Toggle navigation</span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
          </button> 
          <a class="navbar-brand" href="/LaWeedasse/Accueil">LOGO</a> 
        </div> 
      
        <div id="navbar" class="collapse navbar-collapse"> 
        <c:if test="${ sessionScope.connectedUser !=null } " var="resultatTest" >
          <ul class="nav navbar-nav"> 
            <li ><a href="<%=request.getContextPath()%>/Accueil">Accueil</a></li> 
            <li><a href="<%=request.getContextPath()%>/Boutique">Boutique</a></li> 
            <li><a href="<%=request.getContextPath()%>/Contact">Contact</a></li> 
            <li><a href="<%=request.getContextPath()%>/Profil">Profil</a></li> 

          </ul> 
          </c:if>
        </div><!--/.nav-collapse --> 
      </div> 
    </nav> 
 
    <div class="container"> 
 
      <div class="starter-template"> 
      <BR><BR> <BR>  <BR><BR> <BR>
	<c:if test="${sessionScope.connectedUser ==null}">
		<form method="post" action="Accueil">
			<p>
				<label for="nom">Nom:</label> <input type="text" name="nom" id="nom" />
				<label for="mdp">mdp:</label> <input type="text" name="mdp" id="mdp" />
				<c:if test="${ not empty messageErreur }">
				<div style="color: red">${ messageErreur }</div>
				</c:if>	
			</p>
			<input type="submit" />
		</form>
	</c:if>
	<c:if test="${  sessionScope.connectedUser !=null }">
		<p>Bonjour ${sessionScope.nom}</p>
		<ul>
			<li><a href="<%=request.getContextPath()%>/Accueil">Accueil</a></li>
			<li><a href="<%=request.getContextPath()%>/Boutique">Boutique</a></li>
			<li><a href="<%=request.getContextPath()%>/Contact">Contact</a></li>
			<li><a href="<%=request.getContextPath()%>/Profil">Profil</a></li>

		</ul>
	</c:if>
 

</body>
</html>