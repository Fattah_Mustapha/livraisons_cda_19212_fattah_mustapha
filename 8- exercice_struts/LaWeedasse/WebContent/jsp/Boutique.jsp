<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="fr.afpa.dto.WeedDto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="fr.afpa.service.WeedServiceBDD"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> 
 <link crossorigin="anonymous"        href="https://maxcdn.bootstrapcdn.com/
bootstrap/3.3.7/css/bootstrap.min.css" integrity=
"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" 
rel="stylesheet"> 
 <link crossorigin="anonymous"        href="https://maxcdn.bootstrapcdn.com/
bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity=
"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" 
rel="stylesheet"> 
         
 <script crossorigin="anonymous" integrity=
"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" 
src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">
</script> 

<title>La Weedasse</title>
</head>
<body>

  <nav class="navbar navbar-inverse navbar-fixed-top"> 
      <div class="container"> 
      <div class="col-lg-12">
      <img alt="Banniere" src="/static/img/Banweed.png"/>
      </div>
        <div class="navbar-header"> 
          <button type="button" class="navbar-toggle collapsed" data-toggle=
"collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> 
            <span class="sr-only">Toggle navigation</span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
          </button> 
          <a class="navbar-brand" href="/LaWedasse/Accueil">LOGO</a> 
        </div> 
        <div id="navbar" class="collapse navbar-collapse"> 
          <ul class="nav navbar-nav"> 
            <li ><a href="<%=request.getContextPath()%>/Accueil">Accueil</a></li> 
            <li><a href="<%=request.getContextPath()%>/Boutique">Boutique</a></li> 
            <li><a href="<%=request.getContextPath()%>/Contact">Contact</a></li> 
            <li><a href="<%=request.getContextPath()%>/Profil">Profil</a></li> 

          </ul> 
        </div><!--/.nav-collapse --> 
      </div> 
    </nav> 
 
    <div class="container"> 
 
      <div class="starter-template"> 
      <BR><BR> <BR>  <BR><BR> <BR>
	<h1>Bienvenue sur notre Boutique, ${sessionScope.nom} !</h1>
	<a href="<%=request.getContextPath()%>/CreationWeed">Ajouter Un article</a>
	<h2>Voici les articles :</h2>
	<div>
		<table class="table">
			<thead class="grey lighten-2">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Article</th>
					<th scope="col">Modification</th>
					<th scope="col">Suppression</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${empty requestScope.weeds  }">
			
			"Pas de r�sultat" 
			
			</c:if>
				<c:if test="${not empty requestScope.weeds }">
					<c:forEach var="weed" items="${requestScope.weeds}">
						<tr>
							<th scope="row">${weed.id}</th>
							<td><a href="DetailWeed?id=${weed.id }">${weed.nom}</a></td>
							<td><a href="ModifierWeed?id=${weed.id }">Modifier</a></td>
							<td><a href="SupprimerWeed?id=${weed.id }">Supprimer</a></td>
							<td><a href="AjouterAuPanier?id=${weed.id }">Ajouter au panier</a></td>
						</tr>

					</c:forEach>

				</c:if>

			</tbody>
</body>
</html>