create table produit(id_produit numeric not null,
	nom_produit VARCHAR(20),
	description_produit VARCHAR(200),
	categorie_produit VARCHAR(50),
    prix_produit numeric DEFAULT '0',
    image_produit varchar(200) DEFAULT '${pageContext.request.contextPath}/static/img/Homepage_39.jpg',
    quantite_produit numeric(11) DEFAULT '0',
    dateajout time DEFAULT CURRENT_TIMESTAMP,
    primary key (id_produit)) ;


insert into produit (id_produit,nom_produit,description_produit, categorie_produit prix_produit, image_produit,quantite_produit,dateajout) values (1,'Ps4 pro spiderman','Bundle contenant une Playstation 4 pro et le jeu Spiderman.','Jeux Vid�o',329,'${pageContext.request.contextPath}/static/img/ps4.jpg',1, CURRENT_TIMESTAMP);

DROP TABLE IF EXISTS utilisateur
create table utilisateur(id_user numeric not null,
firstname_user varchar(20),
lastname_user varchar(20),
login varchar(50),
email_user varchar (70),
password_user varchar (30),
address_user varchar (70),
primary key (login)) ;

insert into utilisateur (id_user,firstname_user,lastname_user,login,email_user,password_user,address_user) values (1,'brahim','fettih','admin','brahimfettihformation@gmail.com','12345','50 rue de paris 59100 roubaix');
