package factory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigInteger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.junit.jupiter.api.Test;

import persistence.factory.JpaFactory;

class JpaFactoryTest {

    //je recupère l'entityManagerFactory
    final EntityManagerFactory entityManagerFactory = JpaFactory.getEntityManagerFactory();

    @Test
    void test() {
        try {
            assertNotNull(entityManagerFactory);
            assertEquals(entityManagerFactory, JpaFactory.getEntityManagerFactory());
            final EntityManager entityManager = entityManagerFactory.createEntityManager();
            final EntityTransaction entityTransaction = entityManager.getTransaction();
            entityTransaction.begin();
            final BigInteger dumb = (BigInteger) (entityManager.createNativeQuery("Select 1").getSingleResult());
            assertEquals(1L, dumb.longValue());
            entityTransaction.commit();
            entityManager.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
