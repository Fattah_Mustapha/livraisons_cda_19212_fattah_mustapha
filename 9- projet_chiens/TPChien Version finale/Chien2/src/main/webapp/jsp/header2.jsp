<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts.action.ActionErrors"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!--Carousel Wrapper-->
<div id="carousel-example-1z" class="carousel slide carousel-fade pt-4"
	data-ride="carousel">

	<!--Indicators-->
	<ol class="carousel-indicators">
		<li data-target="#carousel-example-1z" data-slide-to="0"
			class="active"></li>
		<li data-target="#carousel-example-1z" data-slide-to="1"></li>
		<li data-target="#carousel-example-1z" data-slide-to="2"></li>
	</ol>
	<!--/.Indicators-->

	<!--Slides-->
	<div class="carousel-inner" role="listbox">

		<!--First slide-->
		<div class="carousel-item active">
			<div class="view"
				style="background-image: url('img/chien_header1.jpg'); background-repeat: no-repeat; background-size: cover;">
				<!-- Mask & flexbox options-->
				<div
					class="mask rgba-black-strong d-flex justify-content-center align-items-center">
					<!-- Content -->
					<div class="text-center white-text mx-5 wow fadeIn">
						<h1 class="mb-4">
							<strong>Bienvenue chez CHIENS-GALAXIE</strong>
						</h1>

						<p>
							<strong>La Galaxie des chiens les plus doux</strong>
						</p>

						<p class="mb-4 d-none d-md-block">
							<strong>Un univers dédié aux chiens pour découvrir leurs
								histoires grâce aux actualités.<br> Des astuces et conseils
								pratiques vous sont également prodigués sur l’adoption,
								l’alimentation, l’éducation pour bien vous occuper de votre
								chien. <br> Vous pourrez aussi découvrir l’identité de
								chaque chien grâce à nos fiches races détaillées.
							</strong>
						</p>

					</div>
					<!-- Content -->

				</div>
				<!-- Mask & flexbox options-->

			</div>
		</div>
		<!--/First slide-->

		<!--Second slide-->
		<div class="carousel-item">
			<div class="view"
				style="background-image: url('img/chien_header2.jpg'); background-repeat: no-repeat; background-size: cover;">

				<!-- Mask & flexbox options-->
				<div
					class="mask rgba-black-strong d-flex justify-content-center align-items-center">

					<!-- Content -->
					<div class="text-center white-text mx-5 wow fadeIn">
						<h1 class="mb-4">
							<strong>Bienvenue chez CHIENS-GALAXIE</strong>
						</h1>

						<p>
							<strong>La Galaxie des chiens les plus doux</strong>
						</p>

						<p class="mb-4 d-none d-md-block">
							<strong>Un univers dédié aux chiens pour découvrir leurs
								histoires grâce aux actualités.<br> Des astuces et conseils
								pratiques vous sont également prodigués sur l’adoption,
								l’alimentation, l’éducation pour bien vous occuper de votre
								chien. <br> Vous pourrez aussi découvrir l’identité de
								chaque chien grâce à nos fiches races détaillées.
							</strong>
						</p>

					</div>
					<!-- Content -->

				</div>
				<!-- Mask & flexbox options-->

			</div>
		</div>
		<!--/Second slide-->

		<!--Third slide-->
		<div class="carousel-item">
			<div class="view"
				style="background-image: url('img/chien_header4.jpg'); background-repeat: no-repeat; background-size: cover;">

				<!-- Mask & flexbox options-->
				<div
					class="mask rgba-black-strong d-flex justify-content-center align-items-center">

					<!-- Content -->
					<div class="text-center white-text mx-5 wow fadeIn">
						<h1 class="mb-4">
							<strong>Bienvenue chez CHIENS-GALAXIE</strong>
						</h1>

						<p>
							<strong>La Galaxie des chiens les plus doux</strong>
						</p>

						<p class="mb-4 d-none d-md-block">
							<strong>Un univers dédié aux chiens pour découvrir leurs
								histoires grâce aux actualités.<br> Des astuces et conseils
								pratiques vous sont également prodigués sur l’adoption,
								l’alimentation, l’éducation pour bien vous occuper de votre
								chien. <br> Vous pourrez aussi découvrir l’identité de
								chaque chien grâce à nos fiches races détaillées.
							</strong>
						</p>

					</div>
					<!-- Content -->

				</div>
				<!-- Mask & flexbox options-->

			</div>
		</div>
		<!--/Third slide-->

	</div>
	<!--/.Slides-->

	<!--Controls-->
	<a class="carousel-control-prev" href="#carousel-example-1z"
		role="button" data-slide="prev"> <span
		class="carousel-control-prev-icon" aria-hidden="true"></span> <span
		class="sr-only">Previous</span>
	</a> <a class="carousel-control-next" href="#carousel-example-1z"
		role="button" data-slide="next"> <span
		class="carousel-control-next-icon" aria-hidden="true"></span> <span
		class="sr-only">Next</span>
	</a>
	<!--/.Controls-->

</div>
<!--/.Carousel Wrapper-->