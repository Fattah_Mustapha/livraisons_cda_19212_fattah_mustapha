<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts.action.ActionErrors"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!--Navbar-->
<nav
	class="navbar navbar-expand-lg navbar-dark mdb-color lighten-3 mt-3 mb-5">
	<!-- Navbar brand -->
	<span class="navbar-brand"><bean:message key="lister.titre" />:</span>
	<!-- Collapse button -->
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#basicExampleNav" aria-controls="basicExampleNav"
		aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<!-- Collapsible content -->
	<div class="collapse navbar-collapse" id="basicExampleNav">
		<!-- Links -->
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active"><a class="nav-link" href="lister.do">All
					<span class="sr-only">(current)</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="lister.do">Moins
					de 2 ans</a></li>
			<li class="nav-item"><a class="nav-link" href="lister.do">de
					2 à 4 ans</a></li>
			<li class="nav-item"><a class="nav-link" href="lister.do">Plus
					de 4 ans </a></li>
		</ul>
		<!-- Links -->

		<form class="form-inline"
			action="${pageContext.request.contextPath}/lister.do">
			<div class="md-form my-0">
				<input class="form-control mr-sm-2" type="text"
					placeholder="Chercher par age" aria-label="Search">
			</div>
		</form>
	</div>
	<!-- Collapsible content -->

</nav>
<!--/.Navbar-->