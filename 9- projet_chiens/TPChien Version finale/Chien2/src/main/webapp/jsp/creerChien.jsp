<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts.action.ActionErrors"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html:html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
<title>Enregister Nouveau Client</title>
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="css/mdb.min.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="css/style.min.css" rel="stylesheet">
</head>
<body class="grey lighten-3">
	<div>
		<!-- Navbar -->
		<jsp:include page="header.jsp"></jsp:include>
		<!-- Navbar -->
	</div>
	<div>Test</div>
	<!-- Heading -->
	<h2 class="my-5 h2 text-center">Enregistrer un nouveau chien</h2>
	<div>
		<html:form action="creer.do" method="post">
			<div>
				<label for="nom">Nom :</label> <input type="text" id="nom"
					name="nom">
			</div>
			<div>
				<label for="couleur">Couleur:</label> <input type="text"
					id="couleur" name="couleur">
			</div>
			<div>
				<label for="age">Age :</label> <input type="text" name="age">
			</div>
			<div>
				<button type="submit">Envoyer</button>
			</div>
	</html:form>
	</div>

	<!--Footer-->
	<jsp:include page="footer.jsp"></jsp:include>
	<!--/.Footer-->
</body>


</html:html>