<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts.action.ActionErrors"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html:html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
<title>Fiches races</title>
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="css/mdb.min.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="css/style.min.css" rel="stylesheet">
</head>
<body class="grey lighten-3">
	<div>
		<!-- Navbar -->
		<jsp:include page="header.jsp"></jsp:include>
		<!-- Navbar -->
	</div>
	<div>Test</div>
	<!-- Heading -->
	<h2 class="my-5 h2 text-center">Fiches races</h2>
	<div>
		<h2>Praesent laoreet</h2>
		Aliquam erat volutpat. Praesent laoreet ante vitae diam eleifend
		consequat. In imperdiet, quam ac consectetur hendrerit, turpis magna
		euismod metus, et lobortis augue felis sed turpis.
	</div>

	<div>
		<h2>Duis pellentesque</h2>
		Quisque imperdiet congue lectus sit amet eleifend. Duis pellentesque
		nunc purus, a aliquam nisi pulvinar in. Donec semper nunc vitae
		interdum dapibus. Sed condimentum ultricies enim, ac hendrerit urna
		suscipit quis. In hac habitasse platea dictumst. Aliquam aliquet,
		magna at pretium tempus, nibh justo vehicula nisl, ut tempus erat
		risus at erat. Nullam sit amet ultrices metus. Ut et dui molestie,
		tempor nibh ac, ornare sapien.
	</div>

	<div>
		<h2>Etiam tristique</h2>
		Maecenas in nibh vitae massa iaculis euismod. Etiam tristique
		tincidunt velit, vitae scelerisque quam euismod ac. Nullam ac metus
		tempor, faucibus purus a, lacinia risus. Nunc tortor mi, feugiat sit
		amet justo vitae, ornare scelerisque velit. Phasellus lacinia justo a
		auctor pellentesque.
	</div>

	<!--Footer-->
	<jsp:include page="footer.jsp"></jsp:include>
	<!--/.Footer-->
</body>


</html:html>