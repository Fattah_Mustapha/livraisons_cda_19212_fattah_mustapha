<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts.action.ActionErrors"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!-- Navbar -->
<nav
	class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
	<div class="container">
		<!-- Brand -->
		<a class="navbar-brand waves-effect" href="lister.do">
			<strong class="blue-text">CHIENS_GALAXIE</strong>
		</a>

		<!-- Collapse -->
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<!-- Links -->
		<div class="collapse navbar-collapse" id="navbarSupportedContent">

			<!-- Left -->
			<ul class="navbar-nav mr-auto">
				<li class="nav-item"><a class="nav-link waves-effect" href="accueil.do">Accueil</a></li>
				<li class="nav-item"><a class="nav-link waves-effect" href="creation_chien.do" >Enregister un nouveau Chien</a></li>
				<li class="nav-item"><a class="nav-link waves-effect" href="lister.do" >Liste des chiens</a></li>
				<li class="nav-item"><a class="nav-link waves-effect" href="fiche_race.do" >Nos fiches races</a></li>
			</ul>

			<!-- Right -->
			<ul class="navbar-nav nav-flex-icons"></ul>
		</div>
	</div>
</nav>
<!-- Navbar -->