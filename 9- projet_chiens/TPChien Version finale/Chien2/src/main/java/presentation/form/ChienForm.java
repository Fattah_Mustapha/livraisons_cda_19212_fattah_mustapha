/**
 * 
 */
package presentation.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import service.ICatalogueService;
import util.ContextConfigurationType;
import util.ContextFactory;

/**
 * @author Must
 *
 */

public class ChienForm extends ActionForm {

	private String nom;
	private String couleur;
	private Integer age;

	/**
	 * 
	 */
	private static final long serialVersionUID = 6438457049925098025L;

	/**
	 * Construteur
	 */
	public ChienForm() {
		super();
	}

	@Override
	public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {

		ICatalogueService clientService = ContextFactory.getContext(ContextConfigurationType.CLASSPATH)
				.getBean(ICatalogueService.class);

		ActionErrors errors = new ActionErrors();

		// nom
		if (nom.isEmpty()) {
			errors.add("nom", new ActionMessage("errors.nom.obligatoire"));
		}

		// prenom
		if (couleur.isEmpty()) {
			errors.add("couleur", new ActionMessage("errors.couleur.obligatoire"));
		}

		if (age.equals(null)) {
			errors.add("age", new ActionMessage("errors.age.obligatoire"));
		}
		
		return errors;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
}
