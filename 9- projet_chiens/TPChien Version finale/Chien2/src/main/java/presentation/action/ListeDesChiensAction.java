package presentation.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.beans.factory.annotation.Autowired;

import service.ICatalogueService;
import service.dto.ChienDto;

public class ListeDesChiensAction extends Action{
	
    @Autowired
    ICatalogueService catalogueService;

    @Override
    public ActionForward execute(final ActionMapping mapping,final  ActionForm form, final HttpServletRequest request,final  HttpServletResponse response) throws Exception {
       List<ChienDto> listeDesChiensDto = catalogueService.listeChiens(); 
       request.setAttribute("listeChiens", listeDesChiensDto);
    	return mapping.findForward("success");
    }
}
