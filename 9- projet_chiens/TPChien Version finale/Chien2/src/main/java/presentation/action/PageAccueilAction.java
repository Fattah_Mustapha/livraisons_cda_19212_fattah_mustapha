package presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.beans.factory.annotation.Autowired;

import service.ICatalogueService;

public class PageAccueilAction extends Action{

    @Autowired
    ICatalogueService catalogueService;

    
    @Override
    public ActionForward execute(final ActionMapping mapping,final  ActionForm form, final HttpServletRequest request,final  HttpServletResponse response) throws Exception {
        
        return mapping.findForward("success");
    }
}
