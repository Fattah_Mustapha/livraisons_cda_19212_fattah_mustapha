/**
 * 
 */
package presentation.action;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.springframework.beans.factory.annotation.Autowired;

import presentation.form.ChienForm;
import service.ICatalogueService;
import service.dto.ChienDto;
import service.dto.mapper.ChienMapper;

/**
 * @author pc
 *
 */
public class CreerChienAction extends Action {
	// recuperer l'application context à partir du serveur (configurer dans le
	// web.xml)
//			WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(this.getServlet().getServletContext());
//			clientService = context.getBean(IClientService.class);
	// ou bien
	// recuperer l'application context pour application java ou les test JUNIT
//			 ApplicationContext contextForJavaOrTestJunit=
//			 ContextFactory.getContext(ContextConfigurationType.CLASSPATH);
//			 chienService = contextForJavaOrTestJunit.getBean(IChienService.class);
	@Autowired
	ICatalogueService chienService;
	
	@Autowired
    ChienMapper   chienMapper;
//	@Autowired
//	DateFormat dateFormat;

	/**
	 * Permet de mapper un Chienform en chienDto
	 * 
	 * @param chienForm
	 * @return chienDto
	 * @throws ParseException
	 */
	public ChienDto mapClientFormToClientDto(final ChienForm chienForm) throws ParseException {

		final ChienDto chienDto = new ChienDto();
		chienDto.setNom(chienForm.getNom());
		chienDto.setCouleur(chienForm.getCouleur());
		chienDto.setAge(chienForm.getAge());
		return chienDto;

	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		// recupere le formulaire chien
		final ChienForm chienForm = (ChienForm) form;

		ChienDto chienDto = mapClientFormToClientDto(chienForm);

		// recupere les info du chienform et on creer le chien en BDD
		final ChienDto newChienDto = chienService.addChien(chienDto);
		// afficher le chien ajouter avec message d'ajout OK/KO
		if (newChienDto == null) {
			final ActionErrors errors = new ActionErrors();
			errors.add("error", new ActionMessage("errors.creation"));
			saveErrors(request, errors);
		} else {
			final ActionMessages messages = new ActionMessages();
			messages.add("creationOK", new ActionMessage("creer.ok", new Object[] { newChienDto.getNom() }));
			saveMessages(request, messages);
		}

		return mapping.findForward("success");
	}

}
