/**
 * 
 */
package persistence.dao.imp;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import persistence.bean.Chien;
import persistence.dao.ICatalogueDAO;
import service.dto.ChienDto;

/**
 * @author Must
 *
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class CatalogueDAOImpl implements ICatalogueDAO {
	
	@PersistenceContext(unitName = "puChien", type = PersistenceContextType.TRANSACTION)
	private EntityManager em;

//	public CatalogueDAOImpl() {
//		EntityManagerFactory emf = Persistence.createEntityManagerFactory("puChien");
//		em = emf.createEntityManager();
//	}
	
	@Override
	public List<Chien> listeChiens() {
		Query req = em.createQuery("select p from Chien p");
		return req.getResultList();
	}

	@Override
	public ChienDto getChien(Integer noPuce) {
		return em.find(ChienDto.class, noPuce);
	}

	@Override
	public void deleteChien(Integer noPuce) {
		ChienDto p = getChien(noPuce);
		em.remove(p);
	}

	@Override
	public void updateChien(Chien p) {
		em.merge(p);
	}

	@Override
	public void addChien(ChienDto chienDto) {
		em.persist(chienDto);
	}

	@Override
	public void addChien(Chien p) {
		em.persist(p);
	}

}
