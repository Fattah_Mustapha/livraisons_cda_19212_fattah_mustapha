package persistence.dao;

import java.util.List;

import persistence.bean.Chien;
import service.dto.ChienDto;

public interface ICatalogueDAO {
	
	void addChien(Chien p);

	public ChienDto getChien(Integer noPuce);
	
	public List<Chien> listeChiens();

	public void deleteChien(Integer noPuce);

	public void updateChien(Chien p);

	public void addChien(ChienDto chienDto);

}
