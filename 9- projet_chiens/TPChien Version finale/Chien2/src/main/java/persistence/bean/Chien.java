/**
 * 
 */
package persistence.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Must
 *
 */

@Entity()
@Table()
@SequenceGenerator(name = "chien_noPuce_seq", initialValue = 1, allocationSize = 1)
public class Chien {

	@Id
	@Column(name = "no_puce")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "chien_noPuce_seq")
	private Integer noPuce;

	@Column(nullable = false)
	private String nom;

	@Column(nullable = false)
	private String couleur;

	@Column(nullable = false)
	private Integer age;

	public Integer getNoPuce() {
		return noPuce;
	}

	public void setNoPuce(Integer noPuce) {
		this.noPuce = noPuce;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
}
