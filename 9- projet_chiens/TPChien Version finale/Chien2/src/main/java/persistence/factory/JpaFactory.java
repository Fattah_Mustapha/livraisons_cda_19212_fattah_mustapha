/**
 * 
 */
package persistence.factory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaFactory {
	private static EntityManagerFactory entityManagerFactory = null;

	/**
	 * Permet de recuperer l'enttityManagerFactory
	 * 
	 * @return entityManagerFactory recupéré
	 */
	public static EntityManagerFactory getEntityManagerFactory() {
		synchronized (JpaFactory.class) {
			if (entityManagerFactory == null) {
				entityManagerFactory = Persistence.createEntityManagerFactory("postgres_bdd");
			}
			return entityManagerFactory;
		}

	}

}
