package service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import persistence.bean.Chien;
import persistence.dao.ICatalogueDAO;
import service.ICatalogueService;
import service.dto.ChienDto;
import service.dto.mapper.ChienMapper;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class CatalogueService implements ICatalogueService {

	@Autowired
	ICatalogueDAO catalogueDAO;
	@Autowired
	ChienMapper clientMapper;

	@Override
	public ChienDto getChien(Integer noPuce) {
		return catalogueDAO.getChien(noPuce);
	}

	@Override
	public void deleteChien(Integer noPuce) {
		catalogueDAO.deleteChien(noPuce);
	}
	
	@Override
	public List<ChienDto> listeChiens() {
		
		return clientMapper.mapListToDto(catalogueDAO.listeChiens());
	}

	@Override
	public void updateChien(Chien p) {
		catalogueDAO.updateChien(p);
	}

	@Override
	public ChienDto addChien(ChienDto chienDto) {
		catalogueDAO.addChien(clientMapper.mapToEntity(chienDto));
		return chienDto;
	}
}
