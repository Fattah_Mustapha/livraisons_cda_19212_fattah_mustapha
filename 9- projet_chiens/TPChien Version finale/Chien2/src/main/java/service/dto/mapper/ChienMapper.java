/**
 * 
 */
package service.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import persistence.bean.Chien;
import service.dto.ChienDto;

/**
 * @author Must
 *
 */
@Component
public class ChienMapper {

	/**
	 * Constructeur
	 */
	public ChienMapper() {
		super();
	}

	/**
	 * Permet de mapper un ClientDto en Client
	 * 
	 * @return client
	 */
	public Chien mapToEntity(final ChienDto chienDto) {
		if(chienDto==null)
			return null;
		final Chien chien = new Chien();
//		chien.setNoPuce(chien.getNoPuce());
		chien.setNom(chienDto.getNom());
		chien.setCouleur(chienDto.getCouleur());
		chien.setAge(chienDto.getAge());

		return chien;
	}

	/**
	 * Permet de mapper un Client en ClientDto
	 * 
	 * @return clientDto
	 */
	public ChienDto mapToDto(final Chien chien) {
		
		if(chien==null)
			return null;
		
		final ChienDto chienDto = new ChienDto();
//		chienDto.setNoPuce(chien.getNoPuce());	
		chienDto.setNom(chien.getNom());
		chienDto.setCouleur(chien.getCouleur());
		chienDto.setAge(chien.getAge());
		
		return chienDto;
	}

	
	
	
	public List<ChienDto> mapListToDto(final List<Chien> listeClient) {
		if(listeClient==null)
			return new ArrayList<ChienDto>();
		final List<ChienDto> listeClientDto = new ArrayList<ChienDto>();

		for (Chien client : listeClient) {
			listeClientDto.add(mapToDto(client));
		}

		return listeClientDto;
	}
	
}
