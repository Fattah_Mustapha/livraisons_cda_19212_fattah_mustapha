/**
 * 
 */
package service.dto;

/**
 * @author Must
 *
 */
public class ChienDto {

	private Integer noPuce;

	private String nom;

	private String couleur;

	private Integer age;

	public Integer getNoPuce() {
		return noPuce;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public void setNoPuce(Integer noPuce) {
		this.noPuce = noPuce;
	}
}
