/**
 * 
 */
package service;

import java.util.List;

import persistence.bean.Chien;
import service.dto.ChienDto;

/**
 * @author Must
 *
 */
public interface ICatalogueService {
	
	public ChienDto addChien(ChienDto chienDto);

	public ChienDto getChien(Integer noPuce);
	
	public List<ChienDto> listeChiens();

	public void deleteChien(Integer noPuce);

	public void updateChien(Chien p);
}