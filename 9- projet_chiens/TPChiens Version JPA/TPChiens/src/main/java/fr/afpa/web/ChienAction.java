package fr.afpa.web;

import java.util.List;

import com.opensymphony.xwork2.ActionSupport;

import fr.afpa.dao.CatalogueDAOJPAImpl;
import fr.afpa.entities.Chien;

public class ChienAction extends ActionSupport{
	public Chien chien = new Chien();
	public List<Chien> chiens;
	public String noPuce;
	public boolean editMode = false;
	private CatalogueDAOJPAImpl test = new CatalogueDAOJPAImpl();
	
	
	public String index() {
		chiens = test.listeChiens();
		return SUCCESS;
	}
	
	public String save() {
		if(editMode==false)
			test.addChien(chien);
		else {
			test.updateChien(chien);
			editMode=false;
			chien = new Chien();
		}
		chiens = test.listeChiens();
		return SUCCESS;
	}
	
	public String delete() {
		test.deleteChien(noPuce);
		chiens = test.listeChiens();
		return SUCCESS;
	}
	
	public String edit() {
		editMode = true;
		chien = test.getChien(noPuce);
		chiens = test.listeChiens();
		return SUCCESS;
	}

}
