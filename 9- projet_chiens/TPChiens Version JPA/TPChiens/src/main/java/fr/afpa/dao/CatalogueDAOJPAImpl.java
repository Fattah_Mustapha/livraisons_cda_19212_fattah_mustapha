package fr.afpa.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afpa.entities.Chien;

public class CatalogueDAOJPAImpl implements ICatalogueDAO{
	@PersistenceContext
	private EntityManager em;
	
	public CatalogueDAOJPAImpl() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("postgres");
		em = emf.createEntityManager();
	}
	
	@Override
	public void addChien(Chien p) {
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		try {
		em.persist(p);
		transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			e.printStackTrace();
		}
	}
	
	@Override
	public List<Chien> listeChiens() {
		Query req = em.createQuery("select p from Chien p");
		return req.getResultList();
	}

	@Override
	public Chien getChien(String noPuce) {
		return em.find(Chien.class, noPuce);
	}

	@Override
	public void deleteChien(String noPuce) {		
		Chien p = getChien(noPuce);
		em.remove(p);
	}

	@Override
	public void updateChien(Chien p) {
		em.merge(p);
	}
	
	public void initialisation() {
		addChien(new Chien("98767", "Maped", "Blanc", 1));
		addChien(new Chien("0987", "Bousti", "Noir", 5));
		addChien(new Chien("12876", "Tyson", "Marron", 9));
	}
}
