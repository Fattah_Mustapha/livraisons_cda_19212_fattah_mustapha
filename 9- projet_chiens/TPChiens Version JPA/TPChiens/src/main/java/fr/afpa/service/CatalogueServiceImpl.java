package fr.afpa.service;

import java.util.List;

import javax.transaction.Transactional;

import fr.afpa.dao.ICatalogueDAO;
import fr.afpa.entities.Chien;

@Transactional
public class CatalogueServiceImpl implements ICatalogueService {
	private ICatalogueDAO dao;

	public void setDao(ICatalogueDAO dao) {
		this.dao = dao;
	}

	@Override
	public void addChien(Chien p) {
		dao.addChien(p);
	}

	@Override
	public List<Chien> listeChiens() {
		return dao.listeChiens();
	}

	@Override
	public Chien getChien(String id) {
		return dao.getChien(id);
	}

	@Override
	public void deleteChien(String id) {
		dao.deleteChien(id);
	}

	@Override
	public void updateChien(Chien p) {
		dao.updateChien(p);
	}

}
