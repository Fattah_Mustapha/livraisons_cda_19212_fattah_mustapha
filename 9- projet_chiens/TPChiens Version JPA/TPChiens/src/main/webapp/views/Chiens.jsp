<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Chiens</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
	<div>
		<s:form action="save" method="post">
			<s:textfield label="Num�ro de puce" name="chien.noPuce"></s:textfield>
			<s:textfield label="Nom" name="chien.nom"></s:textfield>
			<s:textfield label="Couleur" name="chien.couleur"></s:textfield>
			<s:textfield label="Age" name="chien.age"></s:textfield>
			<%-- <s:checkbox label = "Promo" name = "promo"></s:checkbox> --%>
			<s:hidden name="editMode"></s:hidden>
			<s:submit value="Enregistrer"></s:submit>
		</s:form>
	</div>
	<div>
		<table class="table1">
			<tr>
				<th>No de Puce</th>
				<th>Nom</th>
				<th>Couleur</th>
				<th>Age</th>
			</tr>
			<s:iterator value="chiens">
				<tr>
					<td><s:property value="noPuce" /></td>
					<td><s:property value="Nom" /></td>
					<td><s:property value="Couleur" /></td>
					<td><s:property value="Age " /></td>
					<s:url namespace="/" action="delete" var ="lien1">
						<s:param name="noPuce">
						<s:property value="noPuce" />
						</s:param>
					</s:url>
					<s:url namespace="/" action="edit" var ="lien2">
						<s:param name="noPuce">
						<s:property value="noPuce" />
						</s:param>
					</s:url>
					<td><s:a href ="%{lien1}">Supprimer</s:a></td>
					<td><s:a href ="%{lien2}">Editer</s:a></td>
				</tr>
			</s:iterator>
		</table>
	</div>
</body>
</html>