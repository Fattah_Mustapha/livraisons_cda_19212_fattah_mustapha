package fr.afpa.dao;

import java.util.List;

import fr.afpa.entities.Chien;

public interface ICatalogueDAO {
public void addChien(Chien p);
public List<Chien> listeChiens();
public Chien getChien(String noPuce);
public void deleteChien(String noPuce);
public void updateChien(Chien p);
}
