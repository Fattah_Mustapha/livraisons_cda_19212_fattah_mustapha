package fr.afpa.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.afpa.entities.Chien;

public class CatalogueDAOImpl implements ICatalogueDAO{
	private Map<String, Chien> chiens = new HashMap<String, Chien>();
//	Logger log = Logger.getLogger(this.getClass());
	
	@Override
	public void addChien(Chien p) {
		chiens.put(p.getNoPuce(), p);	
	}

	@Override
	public List<Chien> listeChiens() {
		return new ArrayList<Chien>(chiens.values());
	}

	@Override
	public Chien getChien(String id) {
		return chiens.get(id);
	}

	@Override
	public void deleteChien(String id) {
		chiens.remove(id);
	}

	@Override
	public void updateChien(Chien p) {
		chiens.put(p.getNoPuce(), p);
	}

	public void initialisation() {
		addChien(new Chien("98767", "Maped", "Blanc", 1));
		addChien(new Chien("0987", "Bousti", "Noir", 5));
		addChien(new Chien("12876", "Tyson", "Marron", 9));
	}
	
}
