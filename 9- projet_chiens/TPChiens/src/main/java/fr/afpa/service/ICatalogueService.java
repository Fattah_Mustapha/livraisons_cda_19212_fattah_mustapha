package fr.afpa.service;

import java.util.List;

import fr.afpa.entities.Chien;

public interface ICatalogueService {
	public void addChien(Chien p);
	public List<Chien> listeChiens();
	public Chien getChien(String id);
	public void deleteChien(String id);
	public void updateChien(Chien p);
}
