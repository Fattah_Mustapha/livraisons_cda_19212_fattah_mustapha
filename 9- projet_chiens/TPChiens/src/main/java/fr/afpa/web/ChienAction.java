package fr.afpa.web;

import java.util.List;

import com.opensymphony.xwork2.ActionSupport;

import fr.afpa.entities.Chien;
import fr.afpa.service.ICatalogueService;
import fr.afpa.service.SingletonService;

public class ChienAction extends ActionSupport{
	public Chien chien = new Chien();
	public List<Chien> chiens;
	public String noPuce;
	public boolean editMode = false;
	private ICatalogueService service = SingletonService.getService();
	
	
	public String index() {
		chiens = service.listeChiens();
		return SUCCESS;
	}
	
	public String save() {
		if(editMode==false)
			service.addChien(chien);
		else {
			service.updateChien(chien);
			editMode=false;
			chien = new Chien();
		}
		chiens = service.listeChiens();
		return SUCCESS;
	}
	
	public String delete() {
		service.deleteChien(noPuce);
		chiens = service.listeChiens();
		return SUCCESS;
	}
	
	public String edit() {
		editMode = true;
		chien = service.getChien(noPuce);
		chiens = service.listeChiens();
		return SUCCESS;
	}

}
