package fr.afpa.entities;

import java.io.Serializable;

public class Chien implements Serializable {
	
	private String noPuce;
	private String nom;
	private String couleur;
	private Integer age;
	 
	public Chien() {
		super();
		// TODO Auto-generated constructor stub
	}
	
		public Chien(String noPuce, String nom, String couleur, Integer age) {
		super();
		this.noPuce = noPuce;
		this.nom = nom;
		this.couleur = couleur;
		this.age = age;
	}

	public String getNoPuce() {
		return noPuce;
	}
	public void setNoPuce(String noPuce) {
		this.noPuce = noPuce;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCouleur() {
		return couleur;
	}
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	 
	
}
